package operations;

import helpers.Normalization;
import nomen.Monomial;
import nomen.Polynomial;

public class Multiplication {

	public Polynomial result = new Polynomial();

	public Multiplication(Polynomial p1, Polynomial p2) {
		if (p1.monomials.isEmpty() || p2.monomials.isEmpty()) {
			Monomial monom = new Monomial(0, 0);
			result.addMonomial(monom);
		} else {
			Polynomial finalResult = new Polynomial();
			double nr = 0, exp = 0;
			Monomial aux;
			for (Monomial m : p1.monomials) {
				for (Monomial n : p2.monomials) {
					nr = m.getCoefficient() * n.getCoefficient();
					exp = m.getExponent() + n.getExponent();
					aux = new Monomial(nr, exp);
					finalResult.addMonomial(aux);
				}
				finalResult.grade = (int) finalResult.monomials.get(0).getExponent();
			}
			Normalization n = new Normalization(finalResult);
			result = n.normalPolynomial;

		}
	}
}

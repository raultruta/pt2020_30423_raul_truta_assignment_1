package operations;

import nomen.Monomial;
import nomen.Polynomial;

public class Substraction {

	public Polynomial result = new Polynomial();

	public Substraction(Polynomial p1, Polynomial p2) {
		int exponent;
		int j = 0, k = 0;
		Double nr;
		Monomial aux;
		exponent = max(p1.grade, p2.grade);
		while (exponent >= 0) {
			nr = 0.0;
			if (j < p1.monomials.size())
				if (p1.monomials.get(j).getExponent() == exponent) {
					nr = nr + p1.monomials.get(j).getCoefficient();
					j++;
				}
			if (k < p2.monomials.size())
				if (p2.monomials.get(k).getExponent() == exponent) {
					nr = nr - p2.monomials.get(k).getCoefficient();
					k++;
				}
			if (nr != 0) {
				aux = new Monomial(nr, exponent);
				result.addMonomial(aux);
			}
			exponent--;
		}
		if (result.monomials.isEmpty()) {
			result.grade = -1;
		} else {
			result.grade = (int) result.monomials.get(0).getExponent();
		}
	}

	public Polynomial getResult() {
		return result;
	}

	private int max(int i, int j) {
		int m;
		if (i > j)
			m = i;
		else
			m = j;
		return m;
	}

}
package operations;

import nomen.*;

public class Integration {

	public Polynomial result = new Polynomial();	//result of the operation

	public Integration(Polynomial p) {

		for (Monomial m : p.monomials) {
																			
			Double nr = (double) (m.getCoefficient() / (m.getExponent() + 1));
			Double exp = m.getExponent() + 1;									

			Monomial auxMonomial = new Monomial(nr, exp);						
			result.addMonomial(auxMonomial);
		}
	}
}
